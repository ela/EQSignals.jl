# EQSignals
A Julia Package for Earthquake Signal Processing

Basic Functionality

* Integrate Acceleration Records to Velocity and Displacement
* Polynomial Detrending and Baseline Adjustment && Correction
* Solving SDOF and Response Spectra Calculation
* Spectra Fitting and Artificial Earthquake Signal Generation

## Installation
Open julia and type in

    Pkg.clone("EQSignal")

## Basic Usage