type Spectra

    Tstart:: Float64 # 周期控制范围
    Tstop :: Float64 # 周期控制范围
    NT    :: Int64 # 周期点个数
    DMode :: String # 周期点分布形式，可选线性均匀分布或对数均匀分布

    SPT::AbstractArray{Float64,1}
    SPA::AbstractArray{Float64,1}
    SPV::AbstractArray{Float64,1}
    SPD::AbstractArray{Float64,1}
    INDSPA::AbstractArray{Int64,1} # 加速度反应谱值的发生时刻

    SPAT::AbstractArray{Float64,1} # 目标加速度反应谱

    zeta :: Float64 # 阻尼比
    SMethod :: String # 响应求解方法
    pseudo :: Bool # 是否为拟反应谱

end

type EQSignal

    # 定义地震动信号类
    name :: String
    acc  :: AbstractArray{Float64,1}
    vel  :: AbstractArray{Float64,1}
    disp :: AbstractArray{Float64,1}
    t    :: AbstractArray{Float64,1}

    dt   :: Float64
    n    :: Int64
    v0   :: Float64
    d0   :: Float64

    Ia   :: AbstractArray{Float64,1} # 累积Arias强度
    acc0 :: AbstractArray{Float64,1} # 原始加速度记录 (未经过调整)
    vel0 :: AbstractArray{Float64,1} # 合理的速度时程
    disp0:: AbstractArray{Float64,1} # 合理的位移时程
    acc_raw :: AbstractArray{Float64,1} # 原始加速度记录 (未经过调整)
    vel_raw :: AbstractArray{Float64,1} # 原始速度时程 (未经过调整)
    disp_raw:: AbstractArray{Float64,1} # 原始位移时程 (未经过调整)

    paras:: Dict{Symbol,Real} # 特征参数
    sp   :: AbstractArray{Spectra,1} # 多阻尼比反应谱

end